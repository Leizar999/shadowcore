# ShadowCore - World of Warcraft Shadowlands 9.0.2.37474 - Emulator


--------------


* [Introduction](#introduction)
* [Requirements](#requirements)
* [Install](#install)


## Introduction

ShadowCore is a "World Of Warcraft - Shadowlands" emulator.

It is mostly based on TrinityCore, written in c++ and sql.



## Requirements

* Visual Studio 2019
* OpenSSL 1.1.1h
* Boost 1.66
* MySql 5.7.32

* Requirements (500Mb)

https://www.mediafire.com/file/jp3vw0n13734f1e/Core_Requirements_Win64_2021_01_06.rar

* Client Data (5.07Gb)

http://www.mediafire.com/file/uwi9gt30vf6jzi5/ShadowCore_ClientData_enUS_9.0.2.37474_2021_02_21.rar/file

* Minimal Client (1.01Gb)

http://www.mediafire.com/file/jxive68spjca3cd/WOW_SHADOWLANDS_9.0.2.37474_ENUS_SHADOWCORE_MINIMAL.exe



Software requirements are available in the TrinityCore [wiki](https://www.trinitycore.info/display/tc/Requirements) for
Windows, Linux and macOS.



## Install

Detailed installation guides are available in the TrinityCore [wiki](https://www.trinitycore.info/display/tc/Installation+Guide) for
Windows, Linux and macOS.

